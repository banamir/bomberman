package com.banamir.bomberman.messages;

import com.banamir.bomberman.Coord;

import java.util.List;

/**
 * Created by qween on 24.03.16.
 */
public class ExplosionMessage {

    List<Coord> explosionCoords;

    public ExplosionMessage(List<Coord> coords) {
        this.explosionCoords = coords;
    }

    public List<Coord> getExplosionCoords(){
         return explosionCoords;
    }
}
