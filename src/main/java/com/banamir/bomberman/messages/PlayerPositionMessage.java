package com.banamir.bomberman.messages;

import com.banamir.bomberman.Coord;

/**
 * Created by qween on 23.03.16.
 */
public class PlayerPositionMessage {

    private Coord position;

    private String playerId;

    public PlayerPositionMessage(String playerId, Coord position){
        this.playerId = playerId; this.position = position;
    }

    public Coord getPosition(){
        return  position;
    }

    public  String getPlayerId(){
        return playerId;
    }

}
