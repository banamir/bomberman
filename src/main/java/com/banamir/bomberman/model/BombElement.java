package com.banamir.bomberman.model;

import com.banamir.bomberman.Coord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qween on 20.03.16.
 */
public class BombElement extends CombustibleElement {

    final static int attackRange = 3;

    public List<Coord> blowUp() {

        GameBoard board = getBoard();
        int x =  getXCoord(), y = getYCoord();
        List<Coord> destroyeds = new ArrayList();

        destroyeds.add(this.destroy());

        Coord c;
        for(int i = x + 1 ; i <= x + attackRange || i <= board.getWidth(); i++) {
            if((c = tryToDistroy(i,y)) != null) destroyeds.add(c); else break;
        }
        for(int i = x - 1 ; i >= x - attackRange || i > 0; i--) {
            if((c = tryToDistroy(i,y)) != null) destroyeds.add(c); else break;
        }
        for(int i = y + 1 ; i <= y + attackRange || i <= board.getHeight(); i++) {
            if((c = tryToDistroy(x,i)) != null) destroyeds.add(c); else break;
        }
        for(int i = y - 1 ; i >= y - attackRange || i > 0; i--) {
            if((c = tryToDistroy(x,i)) != null) destroyeds.add(c); else break;
        }

        return destroyeds;
    }

    protected Coord tryToDistroy(int i, int j) {
        Coord c = new Coord(i,j);
        GameElement el = getBoard().getElement(c);
        if(el instanceof EmptyElement) return c;
        return (el instanceof CombustibleElement)? ((CombustibleElement) el).destroy() : null;
    }

}
