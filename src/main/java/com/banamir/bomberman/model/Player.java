package com.banamir.bomberman.model;

import com.banamir.bomberman.Coord;

/**
 * Created by qween on 21.03.16.
 */
public class Player extends CombustibleElement{

    public String ID;

    public String name;

    public enum MovingDirection {
        UP,
        DOWN,
        RIGHT,
        LEFT
    };

    Coord pos;

    private static final int INIT_COUNT_BOMB = 1;

    protected boolean destoroyed = false;

    protected int bombCount = INIT_COUNT_BOMB;

    public Coord move(MovingDirection direction){

        int x = pos.x, y =  pos.y;

        switch (direction) {
            case UP:    y++; break;
            case DOWN:  y--; break;
            case RIGHT: x++; break;
            case LEFT:  x--; break;
        }

        if(canMove(x,y)) {
            pos.x = x; pos.y = y;
        }

        return (Coord) pos.clone();
    }

    public BombElement setBomb() {
        bombCount--;
        BombElement bomb = new BombElement();
        getBoard().addElement(this.pos,bomb);
        return bomb;
    }

    public void addBomb() {
        bombCount++;
    }

    protected boolean canMove(int i, int j) {
        if(getBoard() == null)
            throw new IllegalStateException("Board ");

        GameBoard board =  getBoard();

        if(pos.x < 0 || pos.x >= board.getWidth() || pos.y < 0 || pos.y >= board.getHeight())
            return false;

        return board.getElement(new Coord(i,j)) instanceof EmptyElement;
    }

    @Override
    public void setPosition(Coord pos) {
        if(getBoard() == null)
            throw new IllegalStateException("Board ");

        GameBoard board =  getBoard();

        if(pos.x < 0 || pos.x >= board.getWidth() || pos.y < 0 || pos.y >= board.getHeight())
            throw new IllegalArgumentException("Can't set element outside of board.");

        if(!(board.getElement(pos) instanceof EmptyElement))
            throw new IllegalArgumentException("Can't set player in not an empty cell");

        this.pos = pos;
    }

    @Override
    public Coord destroy(){
        destoroyed = true;
        return pos;
    }
}
