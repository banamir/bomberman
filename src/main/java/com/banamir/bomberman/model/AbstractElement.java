package com.banamir.bomberman.model;

/**
 * Created by qween on 20.03.16.
 */
public class AbstractElement implements GameElement {

    private GameBoard board = null;

    public void setBoard(GameBoard board) {

        this.board = board;
    }

    @Override
    public GameBoard getBoard() {
        return board;
    }
}
