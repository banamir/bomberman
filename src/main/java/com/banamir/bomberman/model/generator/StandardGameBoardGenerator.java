package com.banamir.bomberman.model.generator;

import com.banamir.bomberman.Coord;
import com.banamir.bomberman.model.CombustibleElement;
import com.banamir.bomberman.model.FireproofElement;
import com.banamir.bomberman.model.GameBoard;
import com.banamir.bomberman.model.GameElement;

import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by qween on 21.03.16.
 */
public class StandardGameBoardGenerator implements GameBoardGenerator {

    public final static int DEFAULT_COUNT_ELEMENTS = 30;

    public int countElements = DEFAULT_COUNT_ELEMENTS;

    public void  setMaxElement(int count) {
        countElements = count;
    }

    public GameBoard generate() {

        GameBoard board = new GameBoard();

        addFireproofElements(board);

        addCombustibleElements(board);

        return board;
    }

    protected void addFireproofElements(GameBoard board) {

        for(int i = 1; i < board.getWidth(); i = i + 2) {
            for (int j = 1; j< board.getHeight(); j = j +2 ) {
                board.addElement(new Coord(i,j), new FireproofElement());
            }
        }
    }

    protected void addCombustibleElements(GameBoard board) {

        Set<Coord> excluded = new TreeSet();
        excluded.add(new Coord(0,                   board.getHeight() - 1));
        excluded.add(new Coord(0,                   0                    ));
        excluded.add(new Coord(board.getWidth() -1, 0                    ));
        excluded.add(new Coord(board.getWidth() -1, board.getHeight() - 1));

        Random random = new Random(new Date().getTime());
        Coord pos;
        GameElement el;
        for(int i = 0; i < countElements; i++) {

            do {
                pos = new Coord(random.nextInt(board.getWidth() - 1),
                                random.nextInt(board.getHeight() - 1));
               el = board.getElement(pos);
            } while (el != board.getEmpty() || excluded.contains(el));

            board.addElement(pos, new CombustibleElement());
        }


    }
}
