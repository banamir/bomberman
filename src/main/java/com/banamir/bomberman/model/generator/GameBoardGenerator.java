package com.banamir.bomberman.model.generator;

import com.banamir.bomberman.model.GameBoard;

/**
 * Created by qween on 21.03.16.
 */
public interface GameBoardGenerator {


    public GameBoard generate();

}
