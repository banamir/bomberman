package com.banamir.bomberman.model;

import com.banamir.bomberman.Coord;

/**
 * Created by qween on 20.03.16.
 */
public class GameBoard {

    public static final int MIN_BOARD_SIZE = 3;

    GameElement[][] board;

    private final GameElement empty;

    /**
     * Create a default board with
     */
    public GameBoard() {
        this(13, 11);
    }

    /**
     * Create a custom board with specified width and height
     *
     * @param width
     * @param height
     */
    public GameBoard(int width, int height){

        if(width <= MIN_BOARD_SIZE  || height <= MIN_BOARD_SIZE )
            throw new IllegalArgumentException("Can't create game board with width or height less then "+ MIN_BOARD_SIZE + ".");

        board = new GameElement[width][height];

        empty = new EmptyElement(this);

        for(int i = 0; i < width; i++)
            for (int j = 0; i < height; i++)
                board[i][j] = empty;
    }

    /**
     * @return width of board
     */
    public int getWidth() {
        return board.length;
    }

    /**
     * @return height of board
     */
    public int getHeight(){
        return board[0].length;
    }

    /**
     *
     * @return empty element for current board
     */
    public GameElement getEmpty() {
        return empty;
    }

    /**
     *
     * @param pos -- position of element
     * @param element  -- positioned element
     */
    public GameElement addElement(Coord pos, AbstractPositionedElement element) {
        element.setBoard(this);
        element.setPosition(pos);
        return element;
    }

    /**
     *
     * @param pos -- position of element
     * @return elements in position
     */
    public GameElement getElement(Coord pos) {

        if(pos.x < 0 || pos.x >= getWidth() || pos.y < 0 || pos.y >= getHeight())
            throw new IllegalArgumentException("Can't get element outside of board.");

        return board[pos.x][pos.y];
    }


    protected void setElement(Coord pos, GameElement element) {
        board[pos.x][pos.y] = element;
    }

}


