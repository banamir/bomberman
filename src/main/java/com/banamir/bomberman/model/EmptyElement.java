package com.banamir.bomberman.model;

/**
 * Created by qween on 20.03.16.
 */
public class EmptyElement extends AbstractElement {

    GameBoard board;

    EmptyElement(GameBoard board) {
        this.board = board;
    }

    @Override
    public GameBoard getBoard() {
        return board;
    }
}

