package com.banamir.bomberman.model;

import com.banamir.bomberman.Coord;

/**
 * Created by qween on 20.03.16.
 */
public class AbstractPositionedElement extends AbstractElement implements PositionedElement {

    protected Coord pos;

    @Override
    public int getXCoord() {
        return pos.x;
    }

    @Override
    public int getYCoord() {
        return pos.y;
    }

    @Override
    public Coord getPosition() {
        return pos;
    }

    protected void setPosition(Coord pos) {

        if(getBoard() == null)
            throw new IllegalStateException("Board ");

        GameBoard board =  getBoard();

        if(pos.x < 0 || pos.x >= board.getWidth() || pos.y < 0 || pos.y >= board.getHeight())
            throw new IllegalArgumentException("Can't set element outside of board.");

        this.pos.x = pos.x;
        this.pos.y = pos.y;
        board.setElement(pos,this);
    }
}
