package com.banamir.bomberman.model;

import com.banamir.bomberman.Coord;

/**
 * Created by qween on 20.03.16.
 */
public class CombustibleElement extends AbstractPositionedElement {

    protected boolean destroyed = false;

    public Coord destroy() {
        GameBoard board =getBoard();
        Coord c = getPosition();
        board.setElement(c,board.getEmpty());
        destroyed = true;
        return c;
    }

}
