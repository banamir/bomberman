package com.banamir.bomberman.model;

/**
 * Created by qween on 20.03.16.
 */
public interface GameElement {

    void setBoard(GameBoard board);

    GameBoard getBoard();
}
