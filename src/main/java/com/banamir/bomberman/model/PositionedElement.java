package com.banamir.bomberman.model;

import com.banamir.bomberman.Coord;

/**
 * Created by qween on 20.03.16.
 */
public interface PositionedElement extends GameElement {

    public int getXCoord();

    public int getYCoord();

    public Coord getPosition();

}
