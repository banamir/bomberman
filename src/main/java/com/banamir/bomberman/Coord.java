package com.banamir.bomberman;

/**
 * Created by qween on 21.03.16.
 */
public class Coord implements Cloneable {

    public int x;
    public int y;

    public Coord(int x, int y) {
        this.x = x; this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coord)) return false;

        Coord coord = (Coord) o;

        return x == coord.x && y == coord.y;

    }

    @Override
    public Object clone(){
        return new Coord(this.x,this.y);
    }


}
