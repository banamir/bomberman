package com.banamir.bomberman.manager;

import com.banamir.bomberman.Coord;
import com.banamir.bomberman.messages.ExplosionMessage;
import com.banamir.bomberman.model.BombElement;
import com.banamir.bomberman.model.Player;

import java.util.List;

/**
 * Created by qween on 23.03.16.
 */
public class BombTask  {

    public final static int EXPLOSION_DELAY = 3000;

    public BombElement bomb;

    public Player player;

    public BombTask(Player player,BombElement bomb){
        this.player = player; this.bomb =  bomb;
    }

    public ExplosionMessage run() throws InterruptedException {

        Thread.currentThread().sleep(EXPLOSION_DELAY);

        this.player.addBomb();
        List<Coord> coords = this.bomb.blowUp();

        return new ExplosionMessage(coords);
    }

}
