package com.banamir.bomberman.manager;

import com.banamir.bomberman.Coord;
import com.banamir.bomberman.messages.PlayerPositionMessage;
import com.banamir.bomberman.model.GameBoard;
import com.banamir.bomberman.model.Player;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by qween on 23.03.16.
 */
public class PlayerManager  {

    private final static int MOVING_DELAY = 250;

    private final static long BOMB_ADDING_DELAY = 30000;

    private Timer bombAddingTimer = new Timer();

    private long lastMoveTime = new Date().getTime();

    private GameBoard board;
    private Player player;

    public PlayerManager(Player player) {

        this.player = player;
        this.board  = player.getBoard();

    }

    public PlayerPositionMessage move(Player.MovingDirection direction) throws InterruptedException{

        long interval =  new Date().getTime() - lastMoveTime;
        if(interval < MOVING_DELAY)
            Thread.currentThread().sleep(MOVING_DELAY);
        lastMoveTime = new Date().getTime();

        Coord pos = player.move(direction);

        return new PlayerPositionMessage(player.ID, pos);
    }

    public BombTask setBomb() {

        BombTask task = new BombTask(player, player.setBomb());

        return task;
    }

    public void startBombAddingTimer(){

       bombAddingTimer.schedule(new TimerTask() {
           @Override
           public void run() {
               player.addBomb();
           }
       }, 0, BOMB_ADDING_DELAY);
    }
}
